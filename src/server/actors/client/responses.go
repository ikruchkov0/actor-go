package client

import (
	"encoding/json"

	"gitlab.com/ikruchkov0/actor-go/src/server/messages/shared"
)

func fromError(err error) ([]byte, error) {
	errText := err.Error()
	fatalErr := &commandResult{
		Type:      "fatal",
		CommandID: "unknown",
		RequestID: "unknown",
		Error:     &errText,
	}
	json, err := json.Marshal(fatalErr)
	if err != nil {
		return nil, err
	}
	return json, nil
}

func fromRequestError(metadata *sharedmessages.Metadata, err error) ([]byte, error) {
	errText := err.Error()
	cmdErr := &commandResult{
		Type:      "error",
		CommandID: metadata.CommandId,
		RequestID: metadata.RequestId,
		Error:     &errText,
	}
	json, err := json.Marshal(cmdErr)
	if err != nil {
		return nil, err
	}
	return json, nil
}

func fromResult(metadata *sharedmessages.Metadata, result string) ([]byte, error) {
	cmdErr := &commandResult{
		Type:      "result",
		CommandID: metadata.CommandId,
		RequestID: metadata.RequestId,
		Result:    &result,
	}
	json, err := json.Marshal(cmdErr)
	if err != nil {
		return nil, err
	}
	return json, nil
}

func fromGamesList(metadata *sharedmessages.Metadata, gamesListJSON string) ([]byte, error) {
	cmdErr := &commandResult{
		Type:      "games-list",
		CommandID: metadata.CommandId,
		RequestID: metadata.RequestId,
		Result:    &gamesListJSON,
	}
	json, err := json.Marshal(cmdErr)
	if err != nil {
		return nil, err
	}
	return json, nil
}
