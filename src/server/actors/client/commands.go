package client

type clientCommand struct {
	RequestID string `json:"requestId"`
	Type      string `json:"type"`
	Payload   string `json:"payload"`
}

type commandResult struct {
	Type      string  `json:"type"`
	RequestID string  `json:"requestId,omitempty"`
	CommandID string  `json:"commandId,omitempty"`
	Error     *string `json:"error,omitempty"`
	Result    *string `json:"result,omitempty"`
}

type createPlayer struct {
	UserName  string `json:"userName"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type getGames struct {
}

type createGame struct {
	GameName   string `json:"gameName"`
	MaxPlayers int32  `json:"maxPlayers"`
}

type makeBet struct {
	UserName string  `json:"userName"`
	GameName string  `json:"gameName"`
	Amount   float32 `json:"amount"`
	Pick     int32   `json:"pick"`
}

type addMoney struct {
	Amount   float32 `json:"amount"`
	UserName string  `json:"userName"`
}
