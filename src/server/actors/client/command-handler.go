package client

import (
	"fmt"

	"gitlab.com/ikruchkov0/actor-go/src/server/messages/game"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/player"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

func (a *clientActor) handleCommand(metadata *sharedmessages.Metadata, command interface{}) error {
	switch cmd := command.(type) {
	case *createPlayer:
		a.logger.Infof("Creating player %s", cmd.UserName)

		shared.TellPlayer(cmd.UserName, &playermessages.CreatePlayer{
			Metadata: metadata,
			UserName: cmd.UserName,
		})
	case *createGame:
		a.logger.Infof("Create game %s", cmd.GameName)

		shared.TellGame(cmd.GameName, &gamemessages.CreateGame{
			Metadata:          metadata,
			GameName:          cmd.GameName,
			ParticipantsCount: cmd.MaxPlayers,
		})
	case *addMoney:
		a.logger.Infof("Add money %s", cmd.UserName)

		shared.TellPlayer(cmd.UserName, &playermessages.GiveMoney{
			Metadata: metadata,
			Amount:   cmd.Amount,
			Reason:   "Money from UI",
		})
	case *makeBet:
		a.logger.Infof("Make bet %s", cmd.UserName)

		shared.TellPlayer(cmd.UserName, &playermessages.MakeBet{
			Metadata: metadata,
			Amount:   cmd.Amount,
			GameName: cmd.GameName,
			Pick:     cmd.Pick,
		})
	case *getGames:
		a.logger.Infof("Get games")

		shared.TellGamesHub(&gamemessages.GetGames{
			Metadata: metadata,
		})
	default:
		return fmt.Errorf("Unknows command %v", cmd)
	}
	return nil
}
