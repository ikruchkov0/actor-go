package client

import (
	"encoding/json"
	"fmt"

	"gitlab.com/ikruchkov0/actor-go/src/server/messages/client"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

func (a *clientActor) handleMessage(request *clientmessages.Request) ([]byte, error) {
	command, err := parseCommand(request.Data)
	if err != nil {
		return fromError(err)
	}

	metadata := request.Metadata
	metadata.RequestId = command.RequestID

	payload, err := parsePayload(command)
	if err != nil {
		return fromRequestError(metadata, err)
	}

	metadata.CommandId = shared.UUID()

	err = a.handleCommand(metadata, payload)
	if err != nil {
		return fromRequestError(metadata, err)
	}

	return fromResult(metadata, fmt.Sprintf("Accepted %s", command.Type))
}

func parseCommand(msg []byte) (*clientCommand, error) {
	message := &clientCommand{}
	if err := json.Unmarshal(msg, message); err != nil {
		return nil, err
	}
	return message, nil
}

func parsePayload(command *clientCommand) (interface{}, error) {
	var entity interface{}
	switch command.Type {
	case "createPlayer":
		entity = &createPlayer{}
	case "addMoney":
		entity = &addMoney{}
	case "createGame":
		entity = &createGame{}
	case "getGames":
		entity = &getGames{}
	case "makeBet":
		entity = &makeBet{}
	default:
		return nil, fmt.Errorf("Unknows command type %s", command.Type)
	}
	if err := json.Unmarshal([]byte(command.Payload), entity); err != nil {
		return nil, err
	}
	return entity, nil
}
