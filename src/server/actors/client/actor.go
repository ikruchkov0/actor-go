package client

import (
	"fmt"

	"github.com/sirupsen/logrus"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/AsynkronIT/protoactor-go/remote"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/client"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/hub"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

type clientActor struct {
	hub    *actor.PID
	logger *logrus.Entry
}

func (a *clientActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		a.logger.Infof("clientActor started %s \n", context.Self().Id)
	case *clientmessages.Request:
		a.logger.WithField("requst", msg).Infof("request received")
		response, err := a.handleMessage(msg)
		if err != nil {
			panic(err)
		}
		a.hub.Tell(&hubmessages.SendToClient{
			ClientID: msg.Metadata.ClientId,
			Data:     response,
		})
	case *clientmessages.Response:
		a.logger.WithField("response", msg).Infof("response received")
		var response []byte
		var err error
		if msg.Type == shared.CLIENT_OK {
			response, err = fromResult(msg.Metadata, msg.Text)

		} else if msg.Type == shared.CLIENT_ERROR {
			response, err = fromRequestError(msg.Metadata, fmt.Errorf("Command error %s", msg.Text))
		} else if msg.Type == shared.CLIENT_GAMES_LIST {
			response, err = fromGamesList(msg.Metadata, msg.Text)
		}
		if err != nil {
			panic(err)
		}

		a.hub.Tell(&hubmessages.SendToClient{
			ClientID: msg.Metadata.ClientId,
			Data:     response,
		})
	}
}

func getClientActor(logger *logrus.Entry, hub *actor.PID) *actor.Props {
	return actor.FromProducer(func() actor.Actor {
		return &clientActor{
			logger: logger.WithField("actor", "client"),
			hub:    hub,
		}
	})
}

func RegisterActor(logger *logrus.Entry, hub *actor.PID, clientEndpoint string) {
	props := getClientActor(logger, hub)
	remote.Register(clientEndpoint, props)
}
