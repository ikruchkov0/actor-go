package shared

import (
	"encoding/json"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/AsynkronIT/protoactor-go/persistence"
	"github.com/sirupsen/logrus"
)

type baseActor struct {
	persistence.Mixin
	receiver MessageReceiver
	logger   *logrus.Entry
}

type MessageReceiver interface {
	GetState() interface{}
	RecoverState(state interface{})
	GetNewState() interface{}
	ReceiveMessage(message interface{}, log *logrus.Entry)
}

func (a *baseActor) SetMessageReceiver(receiver MessageReceiver) {
	a.receiver = receiver
}

func (a *baseActor) Receive(ctx actor.Context) {
	message := ctx.Message()
	log := a.logger.WithField("message", message)
	switch msg := message.(type) {
	case *actor.Started:
		newState := a.receiver.GetNewState()
		a.receiver.RecoverState(newState)
		log.Infof("Actor started", ctx.Self().Id)
	case *persistence.RequestSnapshot:
		state := a.receiver.GetState()
		log.Infof("Snapshot internal state '%v'", state)
		stateJSON, _ := json.Marshal(state)
		a.PersistSnapshot(NewSnapshot(stateJSON))
	case *Snapshot:
		state := a.receiver.GetNewState()
		if err := json.Unmarshal(msg.GetState(), &state); err != nil {
			panic(err)
		}
		a.receiver.RecoverState(state)
		log.Infof("Recovered from snapshot, internal state changed to '%v'", state)
	case *persistence.ReplayComplete:
		log.Infof("Replay completed, internal state changed to '%v'", a.receiver.GetState())
	case *actor.Terminated:
		log.Infof("Terminated")
	case *actor.Stopped:
		log.Infof("Stopped")
	default:
		a.receiver.ReceiveMessage(message, log)
	}
}

func CreateActorProps(logger *logrus.Entry, receiverProducer func() MessageReceiver) *actor.Props {
	provider := NewProvider(3)
	provider.InitState("persistent", 4, 3)

	return actor.FromProducer(func() actor.Actor {
		return &baseActor{
			logger:   logger,
			receiver: receiverProducer(),
		}
	}).WithMiddleware(persistence.Using(provider))
}
