package player

type playerState struct {
	Created             bool                  `json:"created"`
	UserName            string                `json:"userName"`
	FirstName           string                `json:"firstName"`
	LastName            string                `json:"lastName"`
	Balance             float32               `json:"balance"`
	PendingTransactions transactionCollection `json:"pendingTransactions"`
}
