package player

import (
	"fmt"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/sirupsen/logrus"
	sharedactor "gitlab.com/ikruchkov0/actor-go/src/server/actors/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/game"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/player"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

type playerActor struct {
	state *playerState
}

func (a *playerActor) GetState() interface{} {
	return a.state
}

func (a *playerActor) RecoverState(state interface{}) {
	newState := state.(*playerState)
	a.state = newState
}

func (a *playerActor) GetNewState() interface{} {
	return &playerState{}
}

func (a *playerActor) ReceiveMessage(message interface{}, log *logrus.Entry) {
	switch msg := message.(type) {
	case *playermessages.CreatePlayer:
		log.Infof("Creating player %s", msg.UserName)
		if a.state.Created {
			shared.TellClientError(msg.Metadata, "Already exists")
			return
		}
		a.state.UserName = msg.UserName
		a.state.PendingTransactions = make(transactionCollection, 0)
		a.state.Created = true
		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellClientOk(msg.Metadata, "Player created")
	case *playermessages.GiveMoney:
		log.Infof("Add money %v", msg.Amount)
		if msg.Amount <= 0 {
			shared.TellClientError(msg.Metadata, "Amount should be greater than 0")
			return
		}
		a.state.Balance += msg.Amount
		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellClientOk(msg.Metadata, fmt.Sprintf("%s. Balance %v", msg.Reason, a.state.Balance))
	case *playermessages.MakeBet:
		log.Infof("Make bet %v", msg.Amount)
		if msg.Amount <= 0 {
			shared.TellClientError(msg.Metadata, "Amount should be greater than 0")
			return
		}
		newBalance := a.state.Balance - msg.Amount
		if newBalance < 0 {
			shared.TellClientError(msg.Metadata, "Not enough balance")
			return
		}
		a.state.Balance = newBalance
		var tran *transaction
		tran, a.state.PendingTransactions = a.state.PendingTransactions.addTransaction(msg.Amount)
		shared.TellGame(msg.GameName, &gamemessages.OfferBet{
			Metadata:      msg.Metadata,
			GameName:      msg.GameName,
			UserName:      a.state.UserName,
			BetAmount:     msg.Amount,
			Pick:          msg.Pick,
			TransactionId: tran.ID,
		})
		shared.TellClientOk(msg.Metadata, fmt.Sprintf("Bet amount reserved. Balance %v", a.state.Balance))
	case *playermessages.BetAccepted:
		log.Infof("Bet accepted %v", msg.TransactionId)

		a.state.PendingTransactions = a.state.PendingTransactions.removeTransaction(msg.TransactionId)
		shared.TellClientOk(msg.Metadata, fmt.Sprintf("Bet accepted. Balance %v", a.state.Balance))
	case *playermessages.BetRejected:
		log.Infof("Bet rejected %v", msg.TransactionId)

		tran := a.state.PendingTransactions.getTransaction(msg.TransactionId)
		if tran == nil {
			shared.TellClientError(msg.Metadata, fmt.Sprintf("Transaction %s not found", msg.TransactionId))
			return
		}
		a.state.Balance += tran.Amount
		a.state.PendingTransactions = a.state.PendingTransactions.removeTransaction(msg.TransactionId)
		shared.TellClientOk(msg.Metadata, fmt.Sprintf("Bet rejected. Balance %v", a.state.Balance))
	}
}

func GetActorProps(logger *logrus.Entry) *actor.Props {
	return sharedactor.CreateActorProps(logger.WithField("actor", "Player"), func() sharedactor.MessageReceiver {
		return &playerActor{}
	})
}
