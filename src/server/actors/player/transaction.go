package player

import "gitlab.com/ikruchkov0/actor-go/src/server/shared"

type transaction struct {
	ID     string  `json:"id"`
	Amount float32 `json:"amount"`
}

type transactionCollection []*transaction

func (c transactionCollection) getTransaction(transactionID string) *transaction {
	for _, t := range c {
		if t.ID == transactionID {
			return t
		}
	}
	return nil
}

func (c transactionCollection) addTransaction(amount float32) (*transaction, transactionCollection) {
	tran := &transaction{
		Amount: amount,
		ID:     shared.UUID(),
	}
	return tran, append(c, tran)
}

func (c transactionCollection) removeTransaction(transactionID string) transactionCollection {
	result := make(transactionCollection, 0)
	for _, t := range c {
		if t.ID == transactionID {
			continue
		}
		result = append(result, t)
	}
	return result
}
