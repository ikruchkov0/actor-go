package gameshub

import (
	"encoding/json"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/sirupsen/logrus"
	sharedactor "gitlab.com/ikruchkov0/actor-go/src/server/actors/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/game"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

type gameState struct {
	Name         string `json:"name"`
	PlayersCount int32  `json:"playersCount"`
	MaxPlayers   int32  `json:"maxPlayers"`
}

type gamesState struct {
	Games []*gameState `json:"games"`
}

type gamesHubActor struct {
	state *gamesState
}

func (a *gamesHubActor) GetState() interface{} {
	return a.state
}

func (a *gamesHubActor) RecoverState(state interface{}) {
	newState := state.(*gamesState)
	a.state = newState
}

func (a *gamesHubActor) GetNewState() interface{} {
	return &gamesState{}
}

func (a *gamesHubActor) ReceiveMessage(message interface{}, log *logrus.Entry) {
	switch msg := message.(type) {
	case *gamemessages.CreateGame:
		log.Infof("Add game to hub %s", msg.GameName)
		gameData := &gameState{
			Name:         msg.GameName,
			PlayersCount: 0,
			MaxPlayers:   msg.ParticipantsCount,
		}
		if a.state.Games == nil {
			a.state.Games = make([]*gameState, 0)
		}
		a.state.Games = append(a.state.Games, gameData)
	case *gamemessages.OfferBet:
		log.Infof("Add bet to games hub %v", msg.BetAmount)
		var gameData *gameState
		for _, g := range a.state.Games {
			if g.Name == msg.GameName {
				gameData = g
				break
			}
		}
		if gameData != nil {
			gameData.PlayersCount++
		}
	case *gamemessages.FinishGame:
		log.Infof("Finish game %v", msg.GameName)
		var gameData *gameState
		for _, g := range a.state.Games {
			if g.Name == msg.GameName {
				gameData = g
				break
			}
		}
		if gameData != nil {
			gameData.PlayersCount = 0
		}
	case *gamemessages.GetGames:
		log.Infof("Get games")
		gamesJSON, _ := json.Marshal(a.state.Games)
		shared.TellClientGamesList(msg.Metadata, string(gamesJSON))
	}
}

func GetActorProps(logger *logrus.Entry) *actor.Props {
	return sharedactor.CreateActorProps(logger.WithField("actor", "games-hub"), func() sharedactor.MessageReceiver {
		return &gamesHubActor{}
	})
}
