package clienthub

import (
	"log"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/sirupsen/logrus"
	wsclient "gitlab.com/ikruchkov0/actor-go/src/server/front/ws-client"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/client"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/hub"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

type clientHubActor struct {
	clients        map[string]wsclient.WsClient
	clientEndpoint string
	logger         *logrus.Entry
}

func (a *clientHubActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		a.logger.Infof("hubActor started %s \n", context.Self().Id)
	case *actor.Stopped:
		a.logger.Infof("hubActor stopped %s \n", context.Self().Id)
	case *actor.Terminated:
		a.logger.Infof("hubActor terminated %s \n", context.Self().Id)
	case *hubmessages.RegisterClient:
		a.logger.Infof("register client %s \n", msg.ClientID)
		client := wsclient.NewWsClient(msg.Connection)
		go client.StartSending()
		go client.StartReceiving()
		a.clients[msg.ClientID] = client
		go a.listeningLoop(msg.ClientID, client.ReadChan())
		client.SetCloseHandler(func() error {
			context.Self().Tell(&hubmessages.UnregisterClient{
				ClientID: msg.ClientID,
			})
			return nil
		})
	case *hubmessages.UnregisterClient:
		a.logger.Infof("unregister client %s \n", msg.ClientID)
		client, found := a.clients[msg.ClientID]
		if !found {
			a.logger.Infof("Client %s is not registered", msg.ClientID)
			return
		}
		client.Close()
		delete(a.clients, msg.ClientID)
	case *hubmessages.SendToClient:
		a.logger.Infof("send to client %s \n", msg.ClientID)
		writeChan := a.clients[msg.ClientID].WriteChan()
		writeChan <- msg.Data
	}
}

func (a *clientHubActor) listeningLoop(clientID string, readChan <-chan []byte) {
	for {
		select {
		case msg, ok := <-readChan:
			if !ok {
				log.Println("read channel closed")
				return
			}

			shared.TellClient(&clientmessages.Request{
				Metadata: &sharedmessages.Metadata{
					ClientId:       clientID,
					CommandId:      "unknown",
					RequestId:      "unknown",
					ClientEndpoint: a.clientEndpoint,
				},
				Data: msg,
			})
		}
	}
}

func CreateActorProps(logger *logrus.Entry, clientEndpoint string) *actor.Props {
	return actor.FromProducer(func() actor.Actor {
		return &clientHubActor{
			clients:        make(map[string]wsclient.WsClient),
			clientEndpoint: clientEndpoint,
			logger:         logger.WithField("actor", "client-hub"),
		}
	})
}
