package game

import "math/rand"

type gameState struct {
	Name       string            `json:"name"`
	Players    playersCollection `json:"players"`
	MaxPlayers int32             `json:"maxPlayers"`
	AverageBet float32           `json:"averageBet"`
}

func (s gameState) CheckPick(pick int32) bool {
	for _, p := range s.Players {
		if p.Pick == pick {
			return false
		}
	}
	return true
}

func (s gameState) Finish() (playersCollection, playersCollection, int32, float32) {
	winNum := s.generateRandomWin()
	winners := make(playersCollection, 0)
	loosers := make(playersCollection, 0)
	sum := float32(0)
	for _, p := range s.Players {
		if p.Pick == winNum {
			winners = append(winners, p)
		} else {
			loosers = append(loosers, p)
		}
		sum += p.Bet
	}
	return winners, loosers, winNum, sum / float32(len(winners))
}

func (s gameState) generateRandomWin() int32 {
	return rand.Int31n(10) + 1
}
