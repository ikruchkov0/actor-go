package game

import "gitlab.com/ikruchkov0/actor-go/src/server/messages/game"

type playersCollection []*participantState

func (c playersCollection) getPlayer(userName string) *participantState {
	for _, p := range c {
		if p.UserName == userName {
			return p
		}
	}
	return nil
}

func (c playersCollection) addPlayer(joinCmd *gamemessages.OfferBet) playersCollection {
	participant := &participantState{
		UserName:           joinCmd.UserName,
		JoinClientID:       joinCmd.Metadata.ClientId,
		JoinCommandID:      joinCmd.Metadata.CommandId,
		JoinClientEndpoint: joinCmd.Metadata.ClientEndpoint,
		Bet:                joinCmd.BetAmount,
		Pick:               joinCmd.Pick,
	}

	return append(c, participant)
}
