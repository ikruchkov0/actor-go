package game

import (
	"fmt"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/sirupsen/logrus"
	sharedactor "gitlab.com/ikruchkov0/actor-go/src/server/actors/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/game"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/player"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

const (
	minPlayersCount = 2
	maxPlayersCount = 11
)

type gameActor struct {
	state *gameState
}

func (a *gameActor) GetState() interface{} {
	return a.state
}

func (a *gameActor) RecoverState(state interface{}) {
	newState := state.(*gameState)
	a.state = newState
}

func (a *gameActor) GetNewState() interface{} {
	return &gameState{}
}

func (a *gameActor) ReceiveMessage(message interface{}, log *logrus.Entry) {
	switch msg := message.(type) {
	case *gamemessages.CreateGame:
		log.Infof("Creating game %s", msg.GameName)
		if a.state.Name != "" {
			shared.TellClientError(msg.Metadata, "Game already created")
			return
		}

		if msg.ParticipantsCount <= minPlayersCount && msg.ParticipantsCount >= maxPlayersCount {
			shared.TellClientError(msg.Metadata, fmt.Sprintf("ParticipantsCount should be greater than %d and less than %d", minPlayersCount, maxPlayersCount))
			return
		}

		a.state.Name = msg.GameName
		a.state.Players = make(playersCollection, 0)
		a.state.MaxPlayers = msg.ParticipantsCount

		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellClientOk(msg.Metadata, "Game created")
		shared.TellGamesHub(msg)
	case *gamemessages.OfferBet:
		log.Infof("Offer bet %v", msg.BetAmount)
		if a.state.Name == "" {
			shared.TellClientError(msg.Metadata, "Game doesn't exist")
			shared.TellPlayer(msg.UserName, &playermessages.BetRejected{
				Metadata:      msg.Metadata,
				TransactionId: msg.TransactionId,
			})
			return
		}
		existingParticipant := a.state.Players.getPlayer(msg.UserName)
		if existingParticipant != nil {
			shared.TellClientError(msg.Metadata, "Player already joined")
			shared.TellPlayer(msg.UserName, &playermessages.BetRejected{
				Metadata:      msg.Metadata,
				TransactionId: msg.TransactionId,
			})
			return
		}

		if msg.BetAmount <= 0 {
			shared.TellClientError(msg.Metadata, "Amount should be greater than 0")
			shared.TellPlayer(msg.UserName, &playermessages.BetRejected{
				Metadata:      msg.Metadata,
				TransactionId: msg.TransactionId,
			})
			return
		}

		if a.state.MaxPlayers <= int32(len(a.state.Players)) {
			shared.TellClientError(msg.Metadata, "Game is full")
			shared.TellPlayer(msg.UserName, &playermessages.BetRejected{
				Metadata:      msg.Metadata,
				TransactionId: msg.TransactionId,
			})
			return
		}

		if !a.state.CheckPick(msg.Pick) {
			shared.TellClientError(msg.Metadata, "Pick occupied")
			shared.TellPlayer(msg.UserName, &playermessages.BetRejected{
				Metadata:      msg.Metadata,
				TransactionId: msg.TransactionId,
			})
			return
		}

		a.state.Players = a.state.Players.addPlayer(msg)

		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellPlayer(msg.UserName, &playermessages.BetAccepted{
			Metadata:      msg.Metadata,
			TransactionId: msg.TransactionId,
		})

		shared.TellGamesHub(msg)

		if a.state.MaxPlayers == int32(len(a.state.Players)) {
			winners, loosers, winNum, prize := a.state.Finish()
			for _, w := range winners {
				shared.TellPlayer(w.UserName, &playermessages.GiveMoney{
					Metadata: w.GetMetadata(),
					Amount:   prize,
					Reason:   "Won game: " + a.state.Name,
				})
			}
			for _, l := range loosers {
				shared.TellClientOk(l.GetMetadata(), fmt.Sprintf("Loose. %d was won number", winNum))
			}
			a.state.Players = make(playersCollection, 0)
			shared.TellGamesHub(&gamemessages.FinishGame{
				Metadata: msg.Metadata,
				GameName: a.state.Name,
			})
		}

	}
}

func GetActorProps(logger *logrus.Entry) *actor.Props {
	return sharedactor.CreateActorProps(logger.WithField("actor", "Game"), func() sharedactor.MessageReceiver {
		return &gameActor{}
	})
}
