package game

import "gitlab.com/ikruchkov0/actor-go/src/server/messages/shared"

type participantState struct {
	UserName           string  `json:"userName"`
	JoinClientID       string  `json:"joinClientId"`
	JoinClientEndpoint string  `json:"joinEndpoint"`
	JoinCommandID      string  `json:"joinCommandId"`
	Pick               int32   `json:"pick"`
	Bet                float32 `json:"bet"`
}

func (p *participantState) GetMetadata() *sharedmessages.Metadata {
	return &sharedmessages.Metadata{
		ClientEndpoint: p.JoinClientEndpoint,
		ClientId:       p.JoinClientID,
		CommandId:      p.JoinCommandID,
	}
}
