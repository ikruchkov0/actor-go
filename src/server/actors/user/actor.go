package user

import (
	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/sirupsen/logrus"
	sharedactor "gitlab.com/ikruchkov0/actor-go/src/server/actors/shared"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/player"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/user"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

type userState struct {
	UserName string `json:"userName"`
	Password string `json:"Password"`
}

type userActor struct {
	state *userState
}

func (a *userActor) GetState() interface{} {
	return a.state
}

func (a *userActor) RecoverState(state interface{}) {
	newState := state.(*userState)
	a.state = newState
}

func (a *userActor) GetNewState() interface{} {
	return &userState{}
}

func (a *userActor) ReceiveMessage(message interface{}, log *logrus.Entry) {
	switch msg := message.(type) {
	case *usermessages.RegisterUser:
		log.Infof("Creating user %s", msg.UserName)
		if a.state.UserName != "" {
			shared.TellClientError(msg.Metadata, "User already created")
			return
		}

		shared.TellPlayer(msg.UserName, &playermessages.CreatePlayer{
			UserName: msg.UserName,
		})

		a.state.UserName = msg.UserName
		a.state.Password = msg.Password

		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellClientOk(msg.Metadata, "User registered")
	case *usermessages.LoginUser:
		log.Infof("Login user %s", msg.UserName)
		if a.state.UserName != msg.UserName {
			shared.TellClientError(msg.Metadata, "Username mismatch")
			return
		}

		if a.state.Password != msg.Password {
			shared.TellClientError(msg.Metadata, "Password mismatch")
			return
		}

		log.Infof("internal state changed to '%v'\n", a.state)
		shared.TellClientOk(msg.Metadata, "User logged in")
	}
}

func GetActorProps(logger *logrus.Entry) *actor.Props {
	return sharedactor.CreateActorProps(logger.WithField("actor", "User"), func() sharedactor.MessageReceiver {
		return &userActor{}
	})
}
