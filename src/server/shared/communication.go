package shared

import (
	"fmt"
	"log"
	"time"

	"github.com/AsynkronIT/protoactor-go/actor"
	"github.com/AsynkronIT/protoactor-go/cluster"
	"github.com/AsynkronIT/protoactor-go/remote"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/client"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/shared"
)

const (
	CLIENT_OK         = "OK"
	CLIENT_ERROR      = "ERROR"
	CLIENT_GAMES_LIST = "games-list"
)

func logResposeCode(res remote.ResponseStatusCode) {
	switch res {
	case remote.ResponseStatusCodeERROR:
		log.Println("ResponseStatusCodeERROR")
	case remote.ResponseStatusCodePROCESSNAMEALREADYEXIST:
		log.Println("ResponseStatusCodePROCESSNAMEALREADYEXIST")
	case remote.ResponseStatusCodeTIMEOUT:
		log.Println("ResponseStatusCodePROCESSNAMEALREADYEXIST")
	case remote.ResponseStatusCodeUNAVAILABLE:
		log.Println("ResponseStatusCodeUNAVAILABLE")
	default:
		log.Println(res)
	}
}

type Message interface {
	GetMetadata() *sharedmessages.Metadata
}

const retryCount = 10
const retryAfter = 2 * time.Millisecond

func getActor(name, kind string) *actor.PID {

	for i := 0; i < retryCount; i++ {
		pid, res := cluster.Get(name, kind)
		if res == remote.ResponseStatusCodeOK {
			return pid
		}
		logResposeCode(res)
		time.Sleep(retryAfter)
	}

	log.Fatalf("Cluster error. Unable to get actor %s %s", name, kind)
	return nil
}

func TellGame(gameName string, msg Message) {
	actorName := fmt.Sprintf("Game-%s", gameName)
	getActor(actorName, "Game").Tell(msg)
}

func TellPlayer(userName string, msg Message) {
	actorName := fmt.Sprintf("Player-%s", userName)
	getActor(actorName, "Player").Tell(msg)
}

func TellUser(userName string, msg Message) {
	actorName := fmt.Sprintf("User-%s", userName)
	getActor(actorName, "User").Tell(msg)
}

func TellClientOk(metadata *sharedmessages.Metadata, text string) {
	tellClientResponse(metadata, CLIENT_OK, text)
}

func TellClientGamesList(metadata *sharedmessages.Metadata, gamesListJSON string) {
	tellClientResponse(metadata, CLIENT_GAMES_LIST, gamesListJSON)
}

func TellClientError(metadata *sharedmessages.Metadata, text string) {
	tellClientResponse(metadata, CLIENT_ERROR, text)
}

func tellClientResponse(metadata *sharedmessages.Metadata, msgType, text string) {
	TellClient(&clientmessages.Response{
		Metadata: metadata,
		Text:     text,
		Type:     msgType,
	})
}

func TellClient(msg Message) {
	metadata := msg.GetMetadata()
	actorName := fmt.Sprintf("Client-%s", metadata.ClientId)
	getActor(actorName, metadata.ClientEndpoint).Tell(msg)
}

func TellGamesHub(msg Message) {
	actorName := "GamesHub"
	getActor(actorName, "GamesHub").Tell(msg)
}
