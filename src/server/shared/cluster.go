package shared

import (
	"fmt"
	"log"

	"github.com/AsynkronIT/protoactor-go/cluster"
	"github.com/AsynkronIT/protoactor-go/cluster/consul"
)

func ConnectToConsul() *consul.ConsulProvider {
	config := getConsulConfig()
	cp, err := consul.NewWithConfig(config)
	if err != nil {
		log.Fatal(err)
	}
	return cp
}

func GetClusterAddress() string {
	port := 8081
	host := GetHostname()
	return fmt.Sprintf("%s:%d", host, port)
}

func StartCluster(address string, consul *consul.ConsulProvider) {
	cluster.Start("actor-go", address, consul)
	log.Printf("Member started on %s\n", address)
}

func StopCluster() {
	log.Printf("Stopping cluster")
	cluster.Shutdown(true)
}
