package shared

import (
	"os"
	"os/signal"
)

func WaitForSignal() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop
}
