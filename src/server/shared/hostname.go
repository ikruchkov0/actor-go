package shared

import (
	"log"
	"os"
)

func GetHostname() string {
	host, err := os.Hostname()
	if err != nil {
		log.Printf("Unable to get hostname from os %s. Fallback to env", err.Error())
		host = os.Getenv("HOSTNAME")
		if host == "" {
			log.Fatalf("'HOSTNAME' env should be specified")
		}
	}
	return host
}
