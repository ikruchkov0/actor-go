package shared

import (
	"fmt"
	"time"

	"github.com/olivere/elastic"
	"github.com/sirupsen/logrus"
	elogrus "gopkg.in/sohlich/elogrus.v3"
)

func CreateLogger() *logrus.Logger {
	hostname := GetHostname()

	log := logrus.New()
	var client *elastic.Client
	var err error
	for elasticRetries := 10; elasticRetries > 0; elasticRetries-- {
		client, err = elastic.NewClient(elastic.SetURL("http://elasticsearch:9200"))
		if err == nil {
			break
		}
		time.Sleep(2 * time.Second)
		fmt.Printf("Unable to connect to elastic: %s. Retry\n", err.Error())
	}
	if err != nil {
		log.Panic(err)
	}

	hook, err := elogrus.NewElasticHook(client, hostname, logrus.DebugLevel, "actor-go")
	if err != nil {
		log.Panic(err)
	}
	log.Hooks.Add(hook)
	return log
}
