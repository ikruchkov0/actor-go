package shared

import (
	"log"
	"os"

	"github.com/hashicorp/consul/api"
)

func getConsulConfig() *api.Config {
	consulAddress := os.Getenv("CONSUL_ADDRESS")
	if consulAddress == "" {
		log.Fatalf("'CONSUL_ADDRESS' env should be specified")
	}
	config := api.DefaultConfig()
	config.Address = consulAddress
	return config
}
