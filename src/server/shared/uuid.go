package shared

import (
	"log"

	uuid "github.com/satori/go.uuid"
)

func UUID() string {
	id, err := uuid.NewV4()
	if err != nil {
		log.Fatal(err)
	}
	return id.String()
}
