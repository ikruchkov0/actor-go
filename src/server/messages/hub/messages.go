package hubmessages

import "github.com/gorilla/websocket"

type RegisterClient struct {
	ClientID   string
	Connection *websocket.Conn
}

type UnregisterClient struct {
	ClientID string
}

type SendToClient struct {
	ClientID string
	Data     []byte
}
