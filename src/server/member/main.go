package main

import (
	"github.com/AsynkronIT/protoactor-go/remote"
	"github.com/sirupsen/logrus"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/game"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/games-hub"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/player"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/user"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

func main() {
	logger := shared.CreateLogger().
		WithFields(logrus.Fields{
			"type": "cluster-member",
		})

	registerActors(logger)
	consul := shared.ConnectToConsul()
	address := shared.GetClusterAddress()
	shared.StartCluster(address, consul)

	logger.Infof("Started")

	shared.WaitForSignal()

	shared.StopCluster()

	logger.Infof("Stopped")
}

func registerActors(logger *logrus.Entry) {
	remote.Register("Player", player.GetActorProps(logger))
	remote.Register("Game", game.GetActorProps(logger))
	remote.Register("User", user.GetActorProps(logger))
	remote.Register("GamesHub", gameshub.GetActorProps(logger))
}
