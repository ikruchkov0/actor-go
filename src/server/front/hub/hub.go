package hub

import (
	"log"

	"github.com/sirupsen/logrus"

	"github.com/AsynkronIT/protoactor-go/actor"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/client-hub"
)

func CreateHub(logger *logrus.Entry, clientEndpoint string) *actor.PID {
	props := clienthub.CreateActorProps(logger, clientEndpoint)

	hubInstance, err := actor.SpawnNamed(props, "HUB")
	if err != nil {
		log.Fatal(err)
	}
	return hubInstance
}
