package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/AsynkronIT/protoactor-go/actor"
)

type Server struct {
	logger *log.Logger
	mux    *http.ServeMux
	hub    *actor.PID
}

func newServer(hub *actor.PID) *Server {
	s := &Server{
		logger: log.New(os.Stdout, "cluster-front", 0),
		mux:    http.NewServeMux(),
		hub:    hub,
	}

	s.mux.HandleFunc("/connect", s.serveWSConnect)

	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}

func CreateServer(address string, hub *actor.PID) *http.Server {
	serv := newServer(hub)
	return &http.Server{Addr: address, Handler: serv}
}

func StartServer(server *http.Server) {
	go func() {
		log.Println("Starting server")
		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()
}

func StopServer(server *http.Server) {
	ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
	cancelFn()
}
