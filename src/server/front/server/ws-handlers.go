package server

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"gitlab.com/ikruchkov0/actor-go/src/server/messages/hub"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		origin := r.Header.Get("Origin")
		log.Println("Origin: ", origin)
		return true
	},
}

func (s *Server) serveWSConnect(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	token := params.Get("token")
	if token == "" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil) // error ignored for sake of simplicity
	if err != nil {
		log.Printf("Unable to upgrade ws connection %s\n", err.Error())
		return
	}

	clientID := shared.UUID()
	go s.handleConnection(clientID, conn)
}

func (s *Server) handleConnection(clientID string, conn *websocket.Conn) {
	s.hub.Tell(&hubmessages.RegisterClient{
		ClientID:   clientID,
		Connection: conn,
	})
}
