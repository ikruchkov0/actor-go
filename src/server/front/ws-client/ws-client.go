package wsclient

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

const maxMessageSize = 512

// Time allowed to read the next pong message from the peer.
const pongWait = 60 * time.Second

// Send pings to peer with this period. Must be less than pongWait.
const pingPeriod = (pongWait * 9) / 10

// Time allowed to write a message to the peer.
const writeWait = 10 * time.Second

type wsClient struct {
	conn      *websocket.Conn
	errChan   chan error
	readChan  chan []byte
	writeChan chan []byte
}

type WsClient interface {
	StartReceiving()
	SetCloseHandler(handler func() error)
	StartSending()
	ReadChan() <-chan []byte
	WriteChan() chan<- []byte
	Close()
}

func NewWsClient(conn *websocket.Conn) WsClient {
	return &wsClient{
		conn:      conn,
		errChan:   make(chan error),
		readChan:  make(chan []byte),
		writeChan: make(chan []byte),
	}
}

func (c *wsClient) ReadChan() <-chan []byte {
	return c.readChan
}

func (c *wsClient) WriteChan() chan<- []byte {
	return c.writeChan
}

func (c *wsClient) StartReceiving() {
	go c.readLoop()
}

func (c *wsClient) SetCloseHandler(handler func() error) {
	c.conn.SetCloseHandler(func(code int, text string) error {
		log.Printf("Connection closed %d %s\n", code, text)
		return handler()
	})
}

func (c *wsClient) StartSending() {
	go c.writeLoop()
}

func (c *wsClient) Close() {
	if c.conn == nil {
		return
	}
	err := c.conn.Close()
	if err != nil {
		log.Printf("Channel close err %s", err.Error())
	}
}

func (c *wsClient) readLoop() {
	defer func() {
		log.Println("Exit read loop")
		c.Close()
		close(c.readChan)
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		log.Println("pong received")
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		select {
		case err := <-c.errChan:
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error received: %v", err)
			}
			return
		default:
			_, msg, err := c.conn.ReadMessage()
			if err != nil {
				c.errChan <- err
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					log.Printf("read error: %v", err)
				}
				return
			}
			c.readChan <- msg
		}
	}
}

func (c *wsClient) writeLoop() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		log.Println("Exit write loop")
		ticker.Stop()
		c.Close()
		close(c.writeChan)
	}()

	for {
		select {
		case err := <-c.errChan:
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error received: %v", err)
			}
			return
		case msg, ok := <-c.writeChan:
			if !ok {
				log.Printf("write channer closed")
				return
			}
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.TextMessage, msg); err != nil {
				c.errChan <- err
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					log.Printf("write error: %v", err)
				}
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				c.errChan <- err
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					log.Printf("write ping error: %v", err)
				}
				return
			}
			log.Println("ping sent")
		}
	}
}
