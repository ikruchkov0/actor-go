package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/ikruchkov0/actor-go/src/server/actors/client"
	"gitlab.com/ikruchkov0/actor-go/src/server/front/hub"

	"gitlab.com/ikruchkov0/actor-go/src/server/front/server"
	"gitlab.com/ikruchkov0/actor-go/src/server/shared"
)

func main() {
	logger := shared.CreateLogger().
		WithFields(logrus.Fields{
			"type": "front",
		})

	consul := shared.ConnectToConsul()
	address := shared.GetClusterAddress()

	clientEndpoint := fmt.Sprintf("Client:%s", address)
	hubInstance := hub.CreateHub(logger, clientEndpoint)
	client.RegisterActor(logger, hubInstance, clientEndpoint)
	shared.StartCluster(address, consul)
	logger.Infof("Cluster member Started")

	httpAddress := getHTTPAddressFromEnv()
	serv := server.CreateServer(httpAddress, hubInstance)
	server.StartServer(serv)
	logger.Infof("HTTP Server Started")

	shared.WaitForSignal()

	server.StopServer(serv)
	logger.Infof("HTTP Server Stopped")
	shared.StopCluster()
	logger.Infof("Cluster member Stopped")
}

func getHTTPAddressFromEnv() string {
	host := shared.GetHostname()
	port := 80
	return fmt.Sprintf("%s:%d", host, port)
}
