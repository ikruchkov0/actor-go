## Описание

Proof of concept асинхронной системы построенной на акторах на основе библиотеки [protoactor](https://github.com/AsynkronIT/protoactor-go).

### Сборка и запуск

Для сборки требуется docker и make. Проверял на Ubuntu 16.04. 

Сборка и запуск осуществляются скриптом ``` run.sh ```.

Для сборки создается отдельный контейнер, в него подтягиваются все необходимые зависимости, первый запуск может занимать достаточно продолжительное время (~ 5 минут).

### Endpoints

- http://localhost:8800/ - само приложение
- http://localhost:8500/ui/ - consul
- http://localhost:5601 - kibana

### Бизнесс задача

Я релизовал простую игру "угадай число":
1. Игрок выбирает или создает игру
2. Ввводит свое имя
3. Добавляет себе валюту
4. Делает ставку и выбирает на какое число поставить
5. Когда в игре все игроки поставили, генерируется случайно число и считается кто выиграл, а кто проиграл

## Техническое описание

Приложение состоит из нод выполняющих бизнесс логику (**member**), нод обрабатывающих webscoket подключение от клиентов (**front**), **consul** для организации кластера для [protoactor](https://github.com/AsynkronIT/protoactor-go) и **ngnix**, который проксирует ws подключения на **front** ноды.


### Кластер акторов

[protoactor](https://github.com/AsynkronIT/protoactor-go) берет на себя работу с кластером нод, на которых выполнюятся акторы. Мы можем передавать сообщения актору по имени, библиотека найдет ноду, на которой есть инстанс актора или запустит его, если инстанса нет, и перешлет сообщение.

Ноды в кластере обмениваются [сообщениями](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/messages) по protobuf. У сообщений есть [metadata](https://gitlab.com/ikruchkov0/actor-go/blob/master/src/server/messages/shared/protos.proto), которая используется для трекинга цепочки реквестов и привязки сообщения к клиенту.

### [Front](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/front)

**Front** нода запускает [**server**](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/front/server) для обработки ws подключений. **Server** отправляет все входящие подключения актору [**client-hub**](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/client-hub), который владеет подключением, регистрирует клиентов и удаляет их, при потере соединения. Дальше **client-hub** передает полученное сообщение актору [**client**](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/client), который парсит его и отправляет дальше в акторы, зарегистрированные в кластере на нодах **member**. Поскольку **client** зарегистрирован в кластере, то любой актор в кластере может вытащить ClientID и отравить ответ конкретному **client**. **Client** формирует ответ клиенту и пересылает его по ws через **client-hub**.

### [Member](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/member)

**Member** регистрирует несколько акторов в кластере.

#### [User](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/user)

Отвечает за регистрацию юзера, хранение его данных и логин его в систему.
При регистрации юзера создается игрок.

#### [Player](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/player)

Отвечает за хранения информации игрока (его баланса, существующих игр), начисляе игроку деньги, резервирует ставки

#### [Game](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/game)

Отвечет за логику игры и хранение информации об игре, регистрирует игроков, принимет их ставки, расчитывает победителя и проигравшего

#### [Games-hub](https://gitlab.com/ikruchkov0/actor-go/tree/master/src/server/actors/games-hub)

Отвечает за аггрегацию инорфмации об играх, обарабтывает сообщения от **game** и возвращает список игр, по запросу **client**. 

**_TODO:_**  Проблемное решение, потому что запросов списка игр будет очень много, в идеале нужно держать список игр в памяти каждой **front** ноды и возвращать просто значение из памяти, но broadcast сообщений по кластеру я еще не сделал.


TODO:

- [x] Реализовать игру "угадай число"
- [ ] Сделать broadcast на все акторы нужного типа в кластере, для обновления списка игр в памяти front нод
- [ ] Сделать хотя бы одну money transfer сагу
- [ ] Сделать сохранение эвентов в persistence storage
- [ ] Реализовать игру "морской бой" на react+redux






